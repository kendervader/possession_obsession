﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEffect : MonoBehaviour
{
    //cooldown and max, set max in the inspector
    float cooldown;
    public float maxCooldown;

    // Update is called once per frame
    void Update()
    {
        //cooldown goes to 0 at the lowest and is subtracted from by time
        cooldown = Mathf.Clamp(cooldown - Time.deltaTime, 0, maxCooldown);
    }

    public void Spawn(GameObject spawn)
    {
        //if the cooldown is done
        if (cooldown <= 0)
        {
            //create the effect that is set in the player contoller (spawn)
            Instantiate(spawn);
            //set the cooldown to its max
            cooldown = maxCooldown;
        }
    }
}
