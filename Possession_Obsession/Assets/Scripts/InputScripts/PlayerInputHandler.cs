﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

public class PlayerInputHandler : MonoBehaviour
{

    private AnalogMovement mover;
    // Start is called before the first frame update
    void Start()
    {
        mover = GetComponent<AnalogMovement>();
    }

    public void OnMove(CallbackContext context)
    {
        if (mover != null)
            mover.SetInputVector(context.ReadValue<Vector2>());
        
        //Debug.Log("Movement");
    }

}
