﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalogMovement : MonoBehaviour
{
    public AnimationCurve speed;
    public float walkSpeedModifier = 1;

    private Rigidbody _rigidbody;
    private Transform _transform;

    private Vector3 _moveDirection = Vector3.zero;
    private Vector2 _inputVector = Vector2.zero;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _transform = transform;
    }

    public void SetInputVector(Vector2 direction)
    {
        _inputVector = direction;
        Debug.Log("Movement");
    }
    

    void Update()
    {
        /*
        _moveDirection = new Vector3(_inputVector.x, 0, _inputVector.y);
        
        var time = (Time.fixedDeltaTime * speed.Evaluate(_moveDirection.magnitude) * walkSpeedModifier);

        _rigidbody.MovePosition(_transform.position + _moveDirection * time);
            
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_moveDirection), 0.15F);
        */
        
        _moveDirection = new Vector3(_inputVector.x, 0, _inputVector.y);
        _moveDirection = transform.TransformDirection(_moveDirection);
        _moveDirection = _moveDirection.normalized * walkSpeedModifier * Time.deltaTime;
        
        _rigidbody.MovePosition(transform.position + _moveDirection);
        
    }
}
